jQuery(document).ready(function($){

    // set license description in tooltip to selected license
    $(document).on('click', '#sell_media_product_type', function(){

        // reset everything
        $('#sell_media_print_size option:first-child').attr('selected', true);
        $('#sell_media_item_size option:first-child').attr('selected', true);
        $('#sell_media_item_license option:first-child').attr('selected', true);
        $('.item_add').prop('disabled', true);
        $('#total').text($('#total').data('price'));
        $('.item_pgroup, .item_size, .item_usage, .item_license').attr('value', '');

        var download = $('#sell_media_download_wrapper');
        var print = $('#sell_media_print_wrapper');

        if ($('#download').is(':checked')){
            $(download).show();
            $(print).hide();
            // distinguish between download and reprint types, pass along to gateway
            $('.item_type').attr('value', 'download');
            if ($('#sell_media_download_wrapper #sell_media_download_size_fieldset').length == 0 && $('#sell_media_download_wrapper #sell_media_download_license_fieldset select').length == 0 ) {
                $('.item_add').prop('disabled', false);
            }
        } else {
            $(download).hide();
            $(print).show();
            $('.item_type').attr('value', 'print');
        }

    });

    // disable add to cart button unless print price selected
    $(document).on('change', '#sell_media_print_size', function(){

        // get price from data attribute, set to #total.item_price
        var sum = $('#sell_media_print_size :selected').data('price');
        $('#total').text(sum);
        $('#total').attr( 'data-price', sum );

        // enable add to cart buttons when print size selected
        if (sum > 0) {
            // $('#sell_media_item_size').removeAttr('required');
            // $('#sell_media_item_license').removeAttr('required');
            $('.item_add').prop('disabled', false);
        }
        else {
            // $('#sell_media_item_size').attr('required');
            // $('#sell_media_item_license').attr('required');
            $('.item_add').prop('disabled', true);
        }

        // set price_group id so it is passed to cart
        var price_group = $('#sell_media_print_size :selected').data('id');
        if ( price_group != null )
            $('.item_pgroup').attr('value', price_group);

    });


     /**
     * Popup resize
     */
    function popup_shipped_process_resize(){
        // assign values to the overlay and dialog box and show overlay and dialog
        var width = $(window).width();
        var height = $(document).height();
        
        $('.sell-media-shipping-process-dialog-box').width(width).height(height)
    }

    /** 
     * Popup
     */
    function popup_shipped_process(message){        
        popup_shipped_process_resize();
        $('.sell-media-shipping-process-dialog-box').addClass('is-visible');
        var dialogTop = $(document).scrollTop() + 25;
        $('.sell-media-shipping-process-dialog-box #sell-media-shipping-process-dialog-box-target').css({top:dialogTop});
    }

    $(document).on( 'click', '#shipping-process', function(e){
        e.preventDefault();
        popup_shipped_process('message');
    } );


    /**
     * Close dialog
     */
    $(document).on('click', '.sell-media-shipping-process-dialog-box', function(event){
        if( $(event.target).is('.close') || $(event.target).is('.sell-media-shipping-process-dialog-box') ) {
            event.preventDefault();
            $(this).removeClass('is-visible');
            $('.sell-media-grid-item').removeClass( 'sell-media-active-popup-item' );
            $('#sell-media-shipping-process-dialog-box-target').removeClass('loaded');
            if( 'sell-media-shipping-process-empty-dialog-box' !== $(this).attr( 'id' ) ){
                $('#sell-media-shipping-process-dialog-box-target .sell-media-shipping-process-dialog-box-content').html('');
            }
        }
    });

    /**
     * Close popup when clicking the esc keyboard button.
     * Next popup on next keybord button.
     * Previous popup on previous keybord button.
     */
    $(document).keyup(function(event){
        // Esc
        if(event.which=='27'){
            $('.sell-media-shipping-process-dialog-box').removeClass('is-visible');
            $('.sell-media-shipping-process-grid-item').removeClass( 'sell-media-shipping-process-active-popup-item' );
            $('#sell-media-shipping-process-dialog-box-target').removeClass('loaded');
            if( 'sell-media-shipping-process-empty-dialog-box' !== $('.sell-media-shipping-process-dialog-box').attr( 'id' ) ){
                $('#sell-media-shipping-process-dialog-box-target .sell-media-shipping-process-dialog-box-content').html('');
            }
        }

        // Next
        if( event.which == '39' ){
            sell_media_popup_next_prev( 'next' );
        }

        // Prev
        if( event.which == '37' ){
            sell_media_popup_next_prev( 'prev' );
        }
    });


});