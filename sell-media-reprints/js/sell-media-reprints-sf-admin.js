jQuery( document ).ready(function( $ ){

    $( document ).on('click', '#sell-media-reprints-price-group .sell-media-price-groups-repeater-add', function( event ){
        var counter = +($('.sell-media-price-groups-row:last').attr('data-index')) + 1 ;
        event.preventDefault();

        html = '<tr class="sell-media-price-groups-row" data-index="'+counter+'">';
            html += '<td>';
                html += '<input type="text" class="" name="new_child['+counter+'][name]" size="24" value="">';
                html += '<p class="description">Name</p>';
            html += '</td>';
            html += '<td>';
                html += '<input type="hidden" name="new_child['+counter+'][parent]" value="'+ $('.sell-media-price-group-parent-id:last').val() + '" />';
                html += '<input type="text" class="small-text" name="new_child['+counter+'][width]" value="">';
                html += '<p class="description">Width</p>';
            html += '</td>';
            html += '<td>';
                html += '<input type="text" class="small-text" name="new_child['+counter+'][height]" value="">';
                html += '<p class="description">Height</p>';
            html += '</td>';
            html += '<td>';
                html += '<span class="description">'+sell_media_price_groups.currency_symbol+'</span>&nbsp;';
                html += '<input type="text" class="small-text" name="new_child['+counter+'][price]" value="">';
                html += '<p class="description">Price</p>';
            html += '</td>';
        html += '</tr>';

        $(this).closest('.sell-media-price-groups-table').append( html );
        counter++;
    });

    $(document).on( 'click', 'input#sell-media-print-limited-edition', function(){
        $('#sell-media-print-limited-edition-qty-field').slideToggle('fast');
    });
});