<?php

/**
 * Select a product type
 *
 * @since 1.0.0
 */
function sell_media_reprints_product_type(){

    global $post;
    $product_id = ( ! empty( $_POST['product_id'] ) ) ? $_POST['product_id'] : $post->ID;
    $post_id = $product_id;
    if ( wp_get_post_parent_id( $product_id ) > 0 ) {
        $post_id = wp_get_post_parent_id( $product_id );
    }
    $get_limit = sell_media_reprints_limited_edition_item_total_qty( $product_id );
    if( false !== $get_limit && 0 >= $get_limit )
        return;

    $meta = get_post_meta( $post_id, '_sell_media_reprints_sell', true );
    $settings = sell_media_get_plugin_options();
    $setting = $settings->reprints_hide_download_tabs;

    $sell_media_labels = sell_media_reprints_label();
    $print_label_s =  $sell_media_labels['print']['singular'] ;
    $print_label_p =  $sell_media_labels['print']['plural'] ;

    /**
     * If the product is set to be sold only as a download,
     * hide prints tab
     */
    if ( 'download' == $meta ) : ?>

        <style>#sell_media_print_wrapper{ display: none; }</style>

    <?php
    /**
     * If the product is set to be sold only as a print,
     * hide prints tab
     */
    elseif ( 'reprint' == $meta ) : ?>

        <style>#sell_media_download_wrapper{ display: none; }</style>

    <?php
    /**
     * If the global setting is set to sell prints only,
     * hide download tab
     */
    elseif ( $setting ) : ?>

        <style>#sell_media_download_wrapper{ display: none; }</style>

    <?php
    /**
     * If the product is to be sold as only a print
     * or the global setting to hide downloads is checked
     * then hide download purchase options.
     */
    else : ?>

        <style>#sell_media_download_wrapper{ display: none; }</style>

        <fieldset id="sell_media_product_type_fieldset">
            <form id="sell_media_product_type">
                <input type="radio" name="type" id="print" value="print" checked />
                <label for="print"><?php echo apply_filters( 'sell_media_reprints_buy_print_text', __( 'Buy a ' . strtolower( $print_label_s ), 'sell_media' ) ); ?></label>
                <br />
                <input type="radio" name="type" id="download" value="download" />
                <label for="download"><?php echo apply_filters( 'sell_media_reprints_buy_download_text', __( 'Buy a download', 'sell_media' ) ); ?></label>
            </form>
        </fieldset>

    <?php endif; ?>

    <div id="sell_media_print_wrapper">
        <fieldset id="sell_media_print_size_fieldset">
            <?php $unit_measurement = ( 'in' == $settings->reprints_unit_measurement ) ? 'inches' : 'centimeters'; ?>
            <label for="sell_media_print_size"><?php echo apply_filters( 'sell_media_reprints_print_size_text', __( ucfirst( $print_label_s ) . ' size', 'sell_media' ) ); ?> <span id="print_desc" class="print_desc sell-media-tooltip" data-tooltip="<?php echo apply_filters( 'sell_media_reprints_print_size_tooltip', __( 'All sizes are listed in ' . $unit_measurement . '. Shipping charges will be applied during checkout.', 'sell_media' ), $settings, $unit_measurement ); ?>"> <?php _e( '(?)', 'sell_media' ); ?></span></label>
            <select id="sell_media_print_size" class="sum item_size">
                <option selected="selected" value="" data-price="0" data-qty="0"><?php echo apply_filters( 'sell_media_reprints_print_select_size_text', __( 'Select a size', 'sell_media' ) ); ?></option>
                <?php
                    $price_groups = sell_media_get_price_groups( $post_id, $taxonomy = 'reprints-price-group' );
                    if ( empty( $price_groups ) ) {
                        echo '<p>' . __( 'This item is not currently assigned a price group.', 'sell_media' ) . '</p>';
                    } else {
                        $sell_media_product = new SellMediaProducts();
                        foreach( $price_groups as $term ) {

                            // Check for children only
                            if ( $term->parent != 0 ) {

                                $width = get_term_meta( $term->term_id, 'width', true );
                                $height = get_term_meta( $term->term_id, 'height', true );
                                $price = $sell_media_product->maybe_add_tax_per_item( get_term_meta( $term->term_id, 'price', true ) );
                                $ship_d = get_term_meta( $term->term_id, 'shipping_domestic', true );
                                $ship_i = get_term_meta( $term->term_id, 'shipping_international', true );

                                echo '<option value="' . $term->name . '" data-id="' . $term->term_id . '" data-price="' . $price . '" data-qty="1" data-size="' . $width . ' x ' . $height . '">' . $term->name  . ' (' . $width . ' x ' . $height . ' ' . $settings->reprints_unit_measurement . ') : ' . sell_media_get_currency_symbol() . sprintf( '%0.2f', $price ) . '</option>';
                            }
                        }
                    }
                ?>
            </select>
        </fieldset>
    </div>
<?php }
add_action( 'sell_media_cart_above_size', 'sell_media_reprints_product_type' );


/**
 * Set shipping rates
 *
 * @since 1.0.0
 */
function sell_media_reprints_set_shipping_rates(){

    $settings = sell_media_get_plugin_options();
    $shipping_rate = 0;

    if ( 'shippingFlatRate' == $settings->reprints_shipping )
        $shipping_rate = $settings->reprints_shipping_flat_rate;
    elseif ( 'shippingQuantityRate' == $settings->reprints_shipping )
        $shipping_rate = $settings->reprints_shipping_quantity_rate;
    else
        $shipping_rate = $settings->reprints_shipping_total_rate;

}
add_action( 'sell_media_checkout_before_cart', 'sell_media_reprints_set_shipping_rates' );

/**
 * Show limited edition information on detail page.
 * @param  init $post_id ID of the item.
 */
function sell_media_reprint_show_limited_edition_info( $post_id ){

    $item_total_qty = sell_media_reprints_limited_edition_item_total_qty( $post_id );
    if( false !== $item_total_qty ){
        $notice = '<div class="sell-media-reprints-le">';
        $notice .= "<strong>" . __( 'Limited Edition: ', 'sell_media' ) . "</strong>";

        if( 0 < $item_total_qty ){
            $notice .= "<strong>";
            $notice .= sprintf( __( '%d remaining', 'sell_media' ), $item_total_qty );
            $notice .= "</strong>";
        }
        else{
            $notice .= "<strong>";
            $notice .= __( 'Sold out.', 'sell_media' );
            $notice .= "</strong>";
        }
        $notice .= '</div>';

        echo apply_filters( 'sell_media_reprint_limitied_edition_notice', $notice, $item_total_qty, $post_id );
    }
}

add_action( 'sell_media_add_to_cart_fields', 'sell_media_reprint_show_limited_edition_info', 9 );
