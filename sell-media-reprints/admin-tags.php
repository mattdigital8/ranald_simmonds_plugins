<?php

/**
 * Various items that need to be done when the admin pages are loaded.
 *
 * @since 1.0.0
 */
function sell_media_reprints_sf_admin_init(){
    global $pagenow, $post;

    if ( ! empty( $pagenow ) && ( $pagenow == 'edit.php' && ! empty( $_GET['tab'] ) && $_GET['tab'] == 'sell_media_size_settings' ) || ( 'post.php' == $pagenow && 'sell_media_item' == $post->post_type ) || ( $pagenow == 'post-new.php' && $_GET['post_type'] == 'sell_media_item' ) ){
        wp_enqueue_script( 'sell-media-reprints-sf-admin-script', plugin_dir_url( __FILE__ ) . 'js/sell-media-reprints-sf-admin.js', array( 'jquery' ) );
        wp_enqueue_style( 'sell-media-reprints-sf-admin-style', plugin_dir_url( __FILE__ ) . 'css/sell-media-reprints-sf-admin.css' );
    }
}
add_action( 'admin_enqueue_scripts', 'sell_media_reprints_sf_admin_init' );


/**
 * Display reprint options inside the options meta box
 *
 * @since 1.2
 */
function sell_media_reprints_options_meta_box( $post_id ){ 

    $sell_media_labels = sell_media_reprints_label();
    $print_label_s =  $sell_media_labels['print']['singular'] ;
    $print_label_p =  $sell_media_labels['print']['plural'] ; ?>

    <div id="sell-media-print-price-group-field" class="sell-media-field">
        <label for="sell-media-print-price-group"><?php echo apply_filters( 'sell_media_reprints_price_group_text', __( 'Pricelist for ' . strtolower( $print_label_p ), 'sell_media' ) ); ?></label>
        <?php
            $args = array(
                'show_option_none' => __( 'Select a pricelist', 'sell_media' ),
                'option_none_value' => 0,
                'name' => 'sell_media_print_price_group',
                'id' => 'sell-media-print-price-group',
                'class' => 'sell-media-print-price-group',
                'taxonomy' => 'reprints-price-group',
                'hierarchical' => true,
                'depth' => 1,
                'hide_empty' => false,
                'selected' => sell_media_get_item_price_group( $post_id, 'reprints-price-group' )
            );
            wp_dropdown_categories( $args );
        ?>
        <span class="desc"><?php printf( __( '<a href="%1$s">Edit | Add New</a>', 'sell_media' ), admin_url() . 'edit.php?post_type=sell_media_item&page=pricelists&tab=reprints-price-group' ); ?></span>
    </div>

    <?php
    $selected = '';
    $custom_setting = get_post_meta( $post_id, '_sell_media_reprints_sell', true );
    $settings = sell_media_get_plugin_options();
    $global_setting = ( $settings->reprints_hide_download_tabs ) ? 'reprint' : 'both';

    $selected = ( $custom_setting ) ? $custom_setting : $global_setting; ?>

    <div id="sell-media-print-selling-field" class="sell-media-field">
        <label for="sell-media-print-selling"><?php _e( 'Selling preferences', 'sell_media' ); ?></label>
        <select name="_sell_media_reprints_sell">
            <option value="both" <?php selected( $selected, 'both' ); ?>><?php _e( 'Sell both', 'sell_media' ); ?></option>
            <option value="reprint" <?php selected( $selected, 'reprint' ); ?>><?php echo apply_filters( 'sell_media_reprints_sell_prints', __( 'Sell ' . strtolower( $print_label_s ), 'sell_media' ) ); ?> </option>
            <option value="download" <?php selected( $selected, 'download' ); ?>><?php _e( 'Sell downloads', 'sell_media' ); ?></option>
        </select>
    </div>

    <?php
    $sell_media_print_limited_edition = get_post_meta( $post_id, '_sell_media_print_limited_edition', true );
    ?>
    <div id="sell-media-print-limited-edition-field" class="sell-media-field">
        <label for="sell-media-print-limited-edition"><?php _e( 'Limited edition', 'sell_media' ); ?></label>
        <input type="checkbox" name="_sell_media_print_limited_edition" id="sell-media-print-limited-edition" value="1" <?php checked( 1, $sell_media_print_limited_edition ); ?> />
    </div>

    <?php
    $sell_media_print_limited_edition_qty = get_post_meta( $post_id, '_sell_media_print_limited_edition_qty', true );
    $limited_edition_qty = ( '' == $sell_media_print_limited_edition_qty && isset( $settings->reprints_default_quantity ) ) ? absint( $settings->reprints_default_quantity ) : absint( $sell_media_print_limited_edition_qty );
    $hide_qty_field = ( '' != $sell_media_print_limited_edition )? '' : 'display:none;'
    ?>
    <div id="sell-media-print-limited-edition-qty-field" class="sell-media-field" style="<?php echo $hide_qty_field; ?>">
        <label for="sell-media-print-limited-edition-qty"><?php _e( 'Quantity limit', 'sell_media' ); ?></label>
        <input type="number" name="_sell_media_print_limited_edition_qty" id="sell-media-print-limited-edition-qty" value="<?php echo $limited_edition_qty; ?>" min="0" />
    </div>

<?php }
add_action( 'sell_media_after_options_meta_box', 'sell_media_reprints_options_meta_box', 10, 1 );

function sell_media_reprints_save_options_meta_box( $post_id ){
    if( isset( $_POST['_sell_media_print_limited_edition'] ) && '' != $_POST['_sell_media_print_limited_edition'] ){
        update_post_meta( $post_id, '_sell_media_print_limited_edition', absint( $_POST['_sell_media_print_limited_edition'] ) );
    }
    else{
        update_post_meta( $post_id, '_sell_media_print_limited_edition', ''  );
    }

    if( 
        isset( $_POST['_sell_media_print_limited_edition'] ) && 
        '' != $_POST['_sell_media_print_limited_edition'] &&
        isset( $_POST['_sell_media_print_limited_edition_qty'] ) && 
        '' != $_POST['_sell_media_print_limited_edition_qty'] 
    )
    {
        update_post_meta( $post_id, '_sell_media_print_limited_edition_qty', absint( $_POST['_sell_media_print_limited_edition_qty'] ) );
    }
    else{
        update_post_meta( $post_id, '_sell_media_print_limited_edition_qty', 0 );   
    }

}

add_action( 'sell_media_extra_meta_save', 'sell_media_reprints_save_options_meta_box' );

/**
 * Adds new meta fields to save
 * @param  [array] $fields [an array of fields]
 * @return [array]         [old fields and new fields]
 */
function sell_media_reprints_meta_box_fields( $fields ) {

    $new_fields = array(
        'sell_media_print_price_group',
        '_sell_media_reprints_sell'
    );

    return array_merge( $fields, $new_fields );
}
add_filter( 'sell_media_meta_box_fields', 'sell_media_reprints_meta_box_fields', 10, 1 );


/**
 * Return reprint price group name
 *
 * @param  $post_id
 * @param  $price_id
 * @return $term_obj->name
 */
function sell_media_reprints_sf_payments_meta( $post_id, $price_id ){
    $term_obj = get_term( $price_id, 'reprints-price-group' );
    if ( ! is_wp_error( $term_obj ) && ! empty( $term_obj ) )
        return ', ' . $term_obj->name;
}
add_filter('sell_media_payment_meta', 'sell_media_reprints_sf_payments_meta', 15, 2);


/**
 * Return reprint price
 *
 * @param  $post_id
 * @param  $price_id
 * @return price
 */
function sell_media_reprints_filter_payments_price( $price_id, $price ){
    $term_obj = get_term( $price_id, 'reprints-price-group' );
    if ( is_wp_error( $term_obj ) ){
        return $price;
    } else {
        return get_term_meta( $term_obj, 'price', true );
    }
}
add_filter( 'sell_media_payment_filtered_price', 'sell_media_reprints_filter_payments_price', 15, 2 );