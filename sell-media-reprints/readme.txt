=== Sell Media Reprints Self Fulfillment ===

Contributors: endortrails
Donate link: http://graphpaperpress.com/?download=reprints-self-fulfillment
Tags: commerce, digital downloads, download, downloads, e-commerce, paypal, photography, sell digital, sell download, selling, sell photos, sell videos, sell media, stock photos, print photos
Requires at least: 4.0
Tested up to: 4.7.2
Stable tag: 2.2.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Reprints Self Fulfillment is an extension for the [Sell Media](http://graphpaperpress.com/plugins/sell-media/) plugin for WordPress. This extension allows you to sell physical prints and charge shipping and handling on a per-item basis.

== Description ==

[Sell Media Reprints Self-fulfillment](http://graphpaperpress.com/?download=reprints-self-fulfillment) is a WordPress extension for the [Sell Media](http://graphpaperpress.com/plugins/sell-media/) plugin that allows anyone to sell and ship reprints of their photographs directly out of their WordPress site.

Features:

*   Create as many size options, i.e., 4x6 Glossy, 4x6 Matte as you want.
*   Email notifications telling you what photographs to print and where to ship.
*   Charge different rates for shipping domestically or internationally.
*   Optionally disable the ability for buyers to purchase file downloads.
*   Set you preferred unit of measurement (inches or centimeters).
*   Sell Limited Editions.


== Installation ==

1. Install this extension just like a normal plugin.
2. Activate the plugin.
3. Visit Sell Media -> Settings -> Pricing Tab and configure the options.
4. Add a flat rate shipping and handling for both domestic and international orders.
5. Sell reprints.
6. Optionally configure each item to sell as limited editions.

== Screenshots ==

1. Reprint size, price and shipping options

== Upgrade Notice ==

* Double check your Sell Media Settings after upgrading

== Changelog ==

= 2.2.1 =
* Added print price group to quick and bulk edit

= 2.2.0 =
* Added selling preferences and print price group fields for add bulk plugin
* Added Print price group to bulk edit page
* Hided Shipping Process in checkout page if no print products in cart
* Fix shipping cost added even only download items in cart 

= 2.1.9 =
* Fix: Shipping rate erroneously added if downloads only selected in gallery

= 2.1.8 =
* Add help text to Default Quantity settings
* Fix styling of Shipping Process dialogue box

= 2.1.7 =
* Fix gallery price group issue.

= 2.1.6 =
* Added shipping process field.
* New hooks.
* Minor bug fixings.

= 2.1.5 =
* Fix continuous update issue.

= 2.1.4 =
* Integration with Tax Display feature in Sell Media 2.1.8

= 2.1.3 =
* Notice on dialog fix
* Show/hide option on item add page.

= 2.1.2 =
* Fix limited edition html markup.

= 2.1.1 =
* Added Limited Edition functionality.

= 2.1 =
* Compatibility with Sell Media 2.1

= 2.0.2 =
* Print text is now filterable using the sell_media_print_text filter hook. For usage see: https://gist.github.com/thadallender/8a6199dc145a19dc4461

= 2.0.1 =
* Missing $post_id on metabox causes notice
* Improved logic of sell prints/downloads/both in dialog

= 2.0 =
* Integrating with Sell Media 2.0

= 1.3.6 =
* Removed default UI from backend
* Add requirements check for Sell Media

= 1.3.5 =
* Display selling options default state from options

= 1.3.4 =
* Disable add to cart until size is selected

= 1.3.3 =
* Streamline disabled cart button checks

= 1.3.2 =
* Support for packages

= 1.3.1 =
* Show size and unit of measurement on selects

= 1.3 =
* Integration with Sell Media 1.8

= 1.2.9 =
* State function name change
* Fixing bug where email sent to seller did not contain detailed info

= 1.2.8 =
* Added setting for default country
* Added default POT file
* Fixed PHP notice
* Country and State are now required fields

= 1.2.7 =

* Update to work with the new settings
* Added price groups

= 1.2.6 =
* Tweak: File name change
* Feature: Option to sell reprints, download only or both is now added to the bulk uploader

= 1.2.5 =

* Various updates to the cart

= 1.2.4 =

* Reprint quantity change not reflecting in cart bug fix

= 1.2.3 =

* Purchase notification email check in right place now
* Purchase page link fix in outgoing emails

= 1.2.2 =

* Only send purchase notification if there is an address associated with purchase

= 1.2.1 =

* Corrected issue that had inches showing as " sometimes and "in" other times

= 1.2 =

* Sellers can now specify "reprint", "download" or "both" for a single item
* Added additional product info to single payments page
* Fixing bug in number fields below 0.9
* Added language folder

= 1.1 =

* Updating email formatting

= 1.0 =

* Initial Release