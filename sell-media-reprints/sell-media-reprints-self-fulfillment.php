<?php

/**
 * Plugin Name: Sell Media Reprints Self Fulfillment
 * Plugin URI: http://graphpaperpress.com/?download=reprints-self-fulfillment
 * Description: An extension for Sell Media that allows customers to purchase prints of your images.
 * Version: 2.2.1
 * Author: Graph Paper Press
 * Author URI: http://graphpaperpress.com
 * Author Email: support@graphpaperpress.com
 * License: GPL
 */

define( 'SELL_MEDIA_REPRINTS_SELF_FULFILLMENT_VERSION', '2.2.1' );

/**
 * Check requirements
 */
function sell_media_reprints_check() {
	$plugin = plugin_basename( __FILE__ );
	$plugin_data = get_plugin_data( __FILE__, false );

	if ( ! class_exists( 'SellMedia' ) ) {
		if ( is_plugin_active( $plugin ) ) {
			deactivate_plugins( $plugin );
			wp_die( '<strong>' . $plugin_data['Name'] . '</strong> requires the Sell Media plugin to work. Please activate it first. <br /><br />Back to the WordPress <a href="' . get_admin_url( null, 'plugins.php' ) . '">Plugins page</a>.' );
		}
	}

}
add_action( 'admin_init', 'sell_media_reprints_check' );

require_once( 'admin-tags.php' );
require_once( 'template-tags.php' );
require_once( 'template-functions.php' );

/**
 * Filter text on cart to make product type references more distinct
 *
 * @since 1.0
 */
function sell_media_reprints_filter_download_size_text(){
	return __( 'Download size', 'sell_media' );
}
add_filter( 'sell_media_download_size_text', 'sell_media_reprints_filter_download_size_text' );

/**
 * Filter text on cart to make product type references more distinct
 *
 * @since 1.0
 */
function sell_media_reprints_filter_download_license_text(){
	return __( 'Download license', 'sell_media' );
}
add_filter( 'sell_media_download_license_text', 'sell_media_reprints_filter_download_license_text' );

/**
 * Filter to make product type default to 'print'
 * This is passed along to gateway and helps us optimize
 * outgoing emails, determine if download links apply
 * and if shipping is required.
 *
 * @since 1.0.5
 */
function sell_media_reprints_set_product_type(){

	if ( $_POST && $_POST['product_id'] ) {
		$product_id = $_POST['product_id'];
	} else {
		global $post;
		$product_id = $post->ID;
		$parent_id = sell_media_get_attachment_parent_id( $product_id );
		$product_id = ( '' === $parent_id ) ? $product_id : $parent_id;
	}
	$custom = get_post_meta( $product_id, '_sell_media_reprints_sell', true );

	if ( 'download' == $custom )
		return 'download'; // do not translate this
	else
		return 'print'; // do not translate this
}
add_filter( 'sell_media_set_product_type', 'sell_media_reprints_set_product_type' );

/**
 * Filter to make shipping address required in PayPal
 *
 * @since 1.0
 */
function sell_media_reprints_shipping(){
	return 2; // 0 optional, 1 hide, 2 required
}
add_filter( 'sell_media_shipping', 'sell_media_reprints_shipping' );


/**
 * Filter the email message sent to admins to contain references to prints
 *
 * @since 1.0
 */
function sell_media_reprints_email_admin_receipt_message(){

	$sell_media_labels = sell_media_reprints_label();
    $print_label_s = $sell_media_labels['print']['singular'];
    $print_label_p = $sell_media_labels['print']['plural'];

	return __( 'If you sold a ' . strtolower( $print_label_s ) . ', be sure to ' . strtolower( $print_label_s ) . ' and ship the order to the address listed above. If the order contained downloadable products, no further action is required. Here are the payment details:', 'sell_media' );
}
add_filter( 'sell_media_email_admin_receipt_message', 'sell_media_reprints_email_admin_receipt_message' );


/**
 * Filter the label shown in admin to include references to print
 *
 * @since 1.0
 */
function sell_media_reprints_download_link_label(){
	return __( 'Download or Print', 'sell_media' );
}
add_filter( 'sell_media_download_link_label', 'sell_media_reprints_download_link_label' );


/**
 * Enqueue scripts on front end
 *
 * @since 1.0
 */
function sell_media_reprints_scripts(){
	wp_register_script( 'sell-media-reprints', plugin_dir_url( __FILE__ ) . 'js/sell-media-reprints.js', array( 'jquery', 'sell_media' ), SELL_MEDIA_REPRINTS_SELF_FULFILLMENT_VERSION );
	wp_enqueue_script( 'sell-media-reprints' );

	wp_enqueue_style( 'sell-media-reprints-style', plugin_dir_url( __FILE__ ) . 'css/sell-media-reprints.css' );

}
add_action( 'wp_enqueue_scripts', 'sell_media_reprints_scripts' );


/**
 * Show shipping info on payment details page if print products are sold
 *
 * @since 1.8
 */
function sell_media_reprints_below_payment_contact_details( $payment_id=null ){

	$p = new SellMediaPayments;
	$address = $p->get_buyer_address( $payment_id );
	$sold_prints = $p->products_include_type( $payment_id, 'print' );

	if ( ! empty( $address ) /* && $sold_prints */ ) {
		$html = '';
		$html .= '<h4>' . __( 'Ship to:', 'sell_media' ) . '</h4>';
		$html .= '<p>' . $p->get_buyer_address( $payment_id ) . '</p>';
		echo $html;
	}
}
add_action( 'sell_media_below_payment_contact_details', 'sell_media_reprints_below_payment_contact_details' );


/**
 * Show shipping details below the products purchased table sent via email and shown on dashboard
 *
 * @since 1.8
 */
function sell_media_reprints_below_products_formatted_table( $post_id=null ){

	$p = new SellMediaPayments;
	$html = '';
	$html .= '<p>' . __( 'If you purchased print products, they will be shipped to the address you supplied during checkout. This address is shown below.', 'sell_media' ) . '</p>';
	$html .= '<p>' . $p->get_buyer_name( $post_id ) . '</p>';
	$html .= '<p>' . $p->get_buyer_address( $post_id ) . '</p>';
	return $html;
}
add_action( 'sell_media_below_products_formatted_table', 'sell_media_reprints_below_products_formatted_table' );


/**
 * Retrieve the options from the database fall back on default
 * options if nothing present.
 *
 * @since 1.0
 */
function sell_media_reprints_sf_options( $options ){
	$sell_media_labels = sell_media_reprints_label();
    $print_label_s =  $sell_media_labels['print']['singular'] ;
    $print_label_p =  $sell_media_labels['print']['plural'] ;

	$tab = 'sell_media_size_settings';
	$section = 'reprints_section';

	$additional_options = array(
		'reprints_hide_download_tabs' => array(
			'tab' => $tab,
			'name' => 'reprints_hide_download_tabs',
			'title' => __('Selling Options','sell_media'),
			'description' => apply_filters( 'sell_media_reprints_selling_options_description', __('Check this if you don\'t want to sell file downloads. Your customers will only be able to buy ' . strtolower( $print_label_s ) . ' products. You can override this on a per-item basis on the Add/Edit Item page.', 'sell_media' ) ),
			'section' => $section,
			'since' => '1.0',
			'id' => $section,
			'default' => '',
			'type' => 'checkbox',
			'valid_options' => array(
				'yes' => array(
					'name' => 'yes',
					'title' => apply_filters( 'sell_media_reprints_selling_options_text', __('Only sell ' . strtolower( $print_label_s ) . ' products','sell_media') )
				)
			)
		),
		'reprints_shipping' => array(
			'tab' => $tab,
			'name' => 'reprints_shipping',
			'title' => __('Shipping Rates','sell_media'),
			'description' => apply_filters( 'sell_media_reprints_shipping_rates_description', __('You have three options for calculating shipping rates: Flat Rate, Quantity Rate, Total Rate. Use Flat Rate to set a basic flat rate shipping to all cart orders containing ' . strtolower( $print_label_p ) . '. Use Quantity Rate to charge a flat rate per ' . strtolower( $print_label_s ) . ' sold (i.e. 3 ' . strtolower( $print_label_p ) . ' that cost $10 each with quantity rate shipping of $1 per ' . strtolower( $print_label_s ) . ' would cost a total of $33). Use Total Rate you want a shipping rate that is a percentage of the total cost of the cart.','sell_media') ),
			'section' => $section,
			'since' => '1.0',
			'id' => $section,
			'default' => 'shippingFlatRate',
			'type' => 'select',
			'valid_options' => array(
				'shippingFlatRate' => array(
					'name' => 'shippingFlatRate',
					'title' => __( 'Flat Rate', 'sell_media' )
				),
				'shippingQuantityRate' => array(
					'name' => 'shippingQuantityRate',
					'title' => __( 'Quantity Rate', 'sell_media' )
				),
				'shippingTotalRate' => array(
					'name' => 'shippingTotalRate',
					'title' => __( 'Total Rate', 'sell_media' )
				)
			)
		),
		'reprints_shipping_flat_rate' => array(
			'tab' => $tab,
			'name' => 'reprints_shipping_flat_rate',
			'title' => __('Shipping - Flat Rate','sell_media'),
			'description' => __( 'The flat rate shipping price applied once to all orders (i.e. 1, 5, 10).', 'sell_media' ),
			'section' => $section,
			'since' => '1.0',
			'id' => $section,
			'default' => '',
			'sanitize' => 'html',
			'type' => 'text'
		),
		'reprints_shipping_quantity_rate' => array(
			'tab' => $tab,
			'name' => 'reprints_shipping_quantity_rate',
			'title' => __('Shipping - Quantity Rate','sell_media'),
			'description' => apply_filters( 'sell_media_reprints_shipping_qty_rates_description', __( 'The shipping price applied individually to all ' . strtolower( $print_label_s ) . ' products in the cart (i.e. 1, 5, 10).', 'sell_media' ) ),
			'section' => $section,
			'since' => '1.0',
			'id' => $section,
			'default' => '',
			'sanitize' => 'html',
			'type' => 'text'
		),
		'reprints_shipping_total_rate' => array(
			'tab' => $tab,
			'name' => 'reprints_shipping_total_rate',
			'title' => __('Shipping - Total Rate','sell_media'),
			'description' => __( 'The percentage of the total cost of the cart for shipping (use .05 for 5 percent, .10 for 10 percent, etc)', 'sell_media' ),
			'section' => $section,
			'since' => '1.0',
			'id' => $section,
			'default' => '',
			'sanitize' => 'html',
			'type' => 'text'
		),
		'reprints_free_shipping_threshold' => array(
			'tab' => $tab,
			'name' => 'reprints_free_shipping_threshold',
			'title' => __('Free shipping minimum spend','sell_media'),
			'description' => __( 'The amount the client needs to spend before free shipping is enabled.', 'sell_media' ),
			'section' => $section,
			'since' => '1.0',
			'id' => $section,
			'default' => '',
			'sanitize' => 'html',
			'type' => 'text'
		),
		'reprints_unit_measurement' => array(
			'tab' => $tab,
			'name' => 'reprints_unit_measurement',
			'title' => __('Unit of Measurement','sell_media'),
			'description' => apply_filters( 'sell_media_reprints_unit_measurement_description', __('The unit of measurement in which ' . strtolower( $print_label_s ) . ' products are sold.','sell_media') ),
			'section' => $section,
			'since' => '1.0',
			'id' => $section,
			'default' => 'inches',
			'type' => 'select',
			'valid_options' => apply_filters( 'sell_media_unit_of_measurements', array(
				'cm' => array(
					'name' => 'cm',
					'title' => __( 'Centimeters (cm)', 'sell_media' )
				),
				'in' => array(
					'name' => 'in',
					'title' => __( 'Inches (in)', 'sell_media' )
				)
			) )
		),
		"reprints_default_price_group" => array(
			"tab" => $tab,
			"name" => "reprints_default_price_group",
			"title" => __("Select Default Price Group", "sell_media"),
			"description" => "",
			"id" => $section,
			"section" => $section,
			"type" => "select",
			"default" => "",
			"valid_options" => sell_media_settings_price_group('reprints-price-group')
		),
		"reprints_default_quantity" => array(
			"tab" => $tab,
			"name" => "reprints_default_quantity",
			"title" => __("Limited Editions Quantity", "sell_media"),
			"description" => __("Set the default quantity of limited edition prints available for each photo you sell. You must enable the Limited Edition option on each product page.", "sell_media"),
			"id" => $section,
			"section" => $section,
			"type" => "number",
			"default" => "",
			"valid_options" => ''
		),
	);

	$options["shipping_process"] = array(
			"tab" => "sell_media_general_settings",
			"name" => "shipping_process",
			"title" => __("Shipping process","sell_media"),
			"description" => __( "These &quot;Shipping Process&quot; will show up on the checkout page.", "sell_media" ),
			"section" => "general_plugin_section_1",
			"since" => "1.0",
			"id" => "general_plugin_section_1",
			"sanitize" => "html",
			"type" => "textarea",
			"default" => ""
		);

	return wp_parse_args( $options, $additional_options );
}
add_filter( 'sell_media_options','sell_media_reprints_sf_options' );


/**
 * Add our section the the size & price tab
 *
 * @since 1.2.7
 * @return array containing our new section to be added
 */
function sell_media_reprints_sf_section( $size_price_tab ){

	$sell_media_labels = sell_media_reprints_label();
    $print_label_s =  $sell_media_labels['print']['singular'] ;
    $print_label_p =  $sell_media_labels['print']['plural'] ;

	$size_price_tab['sections']['reprints_section'] = array(
		'name' => 'reprints_section',
		'title' => apply_filters( 'sell_media_reprints_section_print_title', __( ucfirst($print_label_s ) . ' Products','sell_media') ),
		'description' => ''
		);
	return $size_price_tab;
}
add_filter( 'sell_media_size_price_tab', 'sell_media_reprints_sf_section' );


/**
 * When the plugin is activated we check if there is a previously
 * installed version. If there is we do nothing but return. If not
 * we update the version number.
 *
 * @author Zane Matthew
 * @since 1.0
 */
function sell_media_reprints_sf_activation() {
	$version = get_option('sell_media_reprints_sf_version');

	if ( $version && $version > SELL_MEDIA_REPRINTS_SELF_FULFILLMENT_VERSION ){
		return;
	}

	global $wpdb;
	$current_settings = $wpdb->get_results( "SELECT option_name, option_value FROM {$wpdb->prefix}options WHERE option_name LIKE 'sell_media_reprints_sf';" );
	$tmp = empty( $current_settings ) ? null : maybe_unserialize( $current_settings[0]->option_value );

	if ( empty( $tmp ) ){
		return;
	}

	$size_options = $tmp['size_option'];

	$term_meta_keys = array(
		'width',
		'height',
		'price'
		);

	// Init the reprints price group taxonomy
	sell_media_reprints_sf_price_group();

	$taxonomy = 'reprints-price-group';

	// Add our parent
	$term_parent = wp_insert_term( 'Default', $taxonomy );

	$i = 0;
	if ( ! is_wp_error( $term_parent ) ){

		// Loop over our 'size_options'
		foreach( $size_options as $size_option ){

			/**
			 * Allow for duplicate terms by appending 1, 2, 3, etc. to the
			 * term name
			 */
			$term_exists = term_exists( $size_option['description'], $taxonomy, $term_parent['term_id'] );
			if ( $term_exists ) {
				$i++;
				$term_name = $size_option['description'] . ' ' . $i;
			} else {
				$i = 0;
				$term_name = $size_option['description'];
			}

			// Insert child
			$term_child = wp_insert_term( $term_name, $taxonomy, array( 'parent' => $term_parent['term_id'] ) );

			if ( ! empty( $term_parent['term_id'] ) ){
				foreach( $term_meta_keys as $k ){
					$meta = update_term_meta( $term_child['term_id'], $k, $size_option[ $k ] );
				}
			}
		}
	}

	/**
	 * This snippet is used to flush the cache only for 'price-group'. Without
	 * it this wp bug will exists: https://core.trac.wordpress.org/ticket/14485
	 */
	if ( taxonomy_exists( $taxonomy ) ) {
		wp_cache_set( 'last_changed', time( ) - 1800, 'terms' );
		wp_cache_delete( 'all_ids', $taxonomy );
		wp_cache_delete( 'get', $taxonomy );
		delete_option( $taxonomy . '_children' );
		_get_term_hierarchy( $taxonomy );
	}

	// Update the settings to default to use this term
	$settings = get_option('sell_media_options');
	$term_obj = get_term_by( 'name', 'Default', 'reprints-price-group' );
	if ( empty( $settings['reprints_default_price_group'] ) ){
		$settings['reprints_default_price_group'] = $term_obj->term_id;
		unregister_setting( 'sell_media_options', 'sell_media_options', 'sell_media_plugin_options_validate' );
		update_option( 'sell_media_options', $settings );
	}

	// Update our version number
	update_option( 'sell_media_reprints_sf_version', SELL_MEDIA_REPRINTS_SELF_FULFILLMENT_VERSION );
}
register_activation_hook( __FILE__, 'sell_media_reprints_sf_activation' );


/**
 * When the plugin is deactivated we remove our version number.
 *
 * @author Zane Matthew
 * @since 1
 */
function sell_media_reprints_sf_deactivation(){
	delete_option( 'sell_media_reprints_sf_version' );
}
register_deactivation_hook( __FILE__, 'sell_media_reprints_sf_deactivation' );


load_plugin_textdomain( 'sell_media', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );


/**
 * Add our custom taxonomy
 *
 * @since 1.2.7
 */
function sell_media_reprints_sf_price_group() {

	$sell_media_labels = sell_media_reprints_label();

    $print_label_s = $sell_media_labels['print']['singular'];
    $print_label_p = $sell_media_labels['print']['plural'];

	$labels = array(
		'name' => __( ucfirst( $print_label_s ) . ' Price Group', 'sell_media' ),
		'singular_name' => __( ucfirst( $print_label_s ) .' Group', 'sell_media' ),
		'search_items' => __( 'Search ' . ucfirst( $print_label_s ) . ' Group', 'sell_media' ),
		'popular_items' => __( 'Popular ' . ucfirst( $print_label_s ) . ' Group', 'sell_media' ),
		'all_items' => __( 'All ' . ucfirst( $print_label_s ) . ' Group', 'sell_media' ),
		'parent_item' => __( 'Parent ' . ucfirst( $print_label_s ) . ' Group', 'sell_media' ),
		'parent_item_colon' => __( 'Parent ' . ucfirst( $print_label_s ) . '  Group:', 'sell_media' ),
		'edit_item' => __( 'Edit ' . ucfirst( $print_label_s ) . ' Group', 'sell_media' ),
		'update_item' => __( 'Update ' . ucfirst( $print_label_s ) . ' Group', 'sell_media' ),
		'add_new_item' => __( 'Add New ' . ucfirst( $print_label_s ) . ' Group', 'sell_media' ),
		'new_item_name' => __( 'New ' . ucfirst( $print_label_s ) . ' Group', 'sell_media' ),
		'separate_items_with_commas' => __( 'Separate ' . ucfirst( $print_label_s ) . ' Group with commas', 'sell_media' ),
		'add_or_remove_items' => __( 'Add or remove ' . ucfirst( $print_label_s ) . ' Group', 'sell_media' ),
		'choose_from_most_used' => __( 'Choose from most used ' . ucfirst( $print_label_s ) . ' Group', 'sell_media' ),
		'menu_name' => __( '' . ucfirst( $print_label_s ) . ' Price Group', 'sell_media' ),
	);

	$args = array(
		'labels' => $labels,
		'public' => true,

		'show_in_nav_menus' => false,
		'show_tagcloud' => true,
		'show_admin_column' => true,
		'show_ui' => false,
		'hierarchical' => true,
		'rewrite' => true,
		'query_var' => true
	);

	register_taxonomy( 'reprints-price-group', array('sell_media_item'), $args );

}
add_action( 'init', 'sell_media_reprints_sf_price_group', 9 );

/**
 * Since Print Price Groups are added on the settings page, remove it from the submenu
 * This approach still allows for Bulk Editing
 *
* @since 1.7
*/
function sell_media_reprints_sf_adjust_admin_menu(){
	remove_submenu_page( 'edit.php?post_type=sell_media_item', 'edit-tags.php?taxonomy=reprints-price-group&amp;post_type=sell_media_item' );
}
add_action( 'admin_menu', 'sell_media_reprints_sf_adjust_admin_menu', 999 );

/**
 * Filter the default price copy
 *
 * @since 1.2.7
 * @param $default_copy (string) The default copy shown above prices
 * @param $taxonomy (string) The taxonomy you want to filter the copy for
 */
function sell_media_reprints_sf_price_copy( $default_copy, $taxonomy ){

	$sell_media_labels = sell_media_reprints_label();
    $print_label_s =  $sell_media_labels['print']['singular'] ;
    $print_label_p =  $sell_media_labels['print']['plural'] ;

	return ( $taxonomy == 'reprints-price-group' ) ? __('Add ' . $print_label_s . ' sizes and prices below.','sell_media') : $default_copy;
}
add_filter( 'sell_media_default_price_copy','sell_media_reprints_sf_price_copy', 99, 2 );


/**
 * Filter the default price group copy
 *
 *
 * @since 1.2.7
 * @param $default_copy (string) The default copy shown above prices
 * @param $taxonomy (string) The taxonomy you want to filter the copy for
 */
function sell_media_reprints_sf_price_group_copy( $default_copy, $taxonomy ){

	$sell_media_labels = sell_media_reprints_label();
    $print_label_s =  $sell_media_labels['print']['singular'] ;
    $print_label_p =  $sell_media_labels['print']['plural'] ;

	return ( $taxonomy == 'reprints-price-group' ) ? __('Create a price group, then define specific ' . $print_label_s . ' sizes and prices.','sell_media') : $default_copy;
}
add_filter( 'sell_media_default_price_group_copy','sell_media_reprints_sf_price_group_copy', 99, 2 );


/**
 * Adds the default children when a user first creates a price group
 *
 * @since 1.2.7
 */
function sell_media_reprints_sf_pg_default_children( $default_terms, $taxonomy, $parent_term_id ){

	if ( $taxonomy == 'reprints-price-group' ){

		$max = 1; // The number of defaults to show
		$new_default = null;
		$settings = sell_media_get_plugin_options();

		for( $i = 1; $i <= $max; $i++ ) {
			$names = array(
				'width' => 'new_child[' . $i .'][width]',
				'height' => 'new_child[' . $i .'][height]',
				'price' => 'new_child[' . $i .'][price]'
				);

			$new_default .= '<tr class="sell-media-price-groups-row" data-index="' . $i . '">';
			$new_default .= '<input type="hidden" class="sell-media-price-group-parent-id" name="new_child[' . $i . '][parent]" value="' . $parent_term_id . '" />';
			$new_default .= '<td><input type="text" class="" name="new_child[' . $i . '][name]" size="24" value=""><p class="description">' . __('Name','sell_media') . '</p></td>';
			$new_default .= sell_media_reprints_sf_table_cells( $names );
			$new_default .= '</tr>';
		}
		$default_terms = $new_default;
	}

	return $default_terms;
}
add_filter( 'sell_media_default_price_group_children', 'sell_media_reprints_sf_pg_default_children', 99, 3 );


/**
 * Filter the current term (name) and term meta (height, width, and price),
 * adding additional term meta (shipping and handling).
 *
 * @since 1.2.7
 * @param $taxonomy (string) The taxonomy being passed in by the filter
 * @param $defaults (array) The defaults to filter
 *
 * @return $defaults (array)
 */
function sell_media_reprints_sf_term_meta( $taxonomy, $defaults ){
	if ( $taxonomy == 'reprints-price-group' && ! empty( $defaults ) ){
		foreach( $defaults as $k => $v ){
			foreach( $v['meta'] as $meta ){

				// Just too make sense of it all
				$term_id = $defaults[ $k ]['term_id'];

				$values = array(
					'width' => get_term_meta( $term_id, 'width', true ),
					'height' => get_term_meta( $term_id, 'height', true ),
					'price' => get_term_meta( $term_id, 'price', true )
				);

				$terms_children = 'terms_children[' . $term_id . ']';

				$names = array(
					'width' => $terms_children . '[width]',
					'height' => $terms_children . '[height]',
					'price' => $terms_children . '[price]'
					);

				$new_html = sell_media_reprints_sf_table_cells( $names, $values );
			}
			$defaults[ $k ]['meta']['html'] = $new_html;
		}
	}
	return $defaults;
}
add_filter( 'sell_media_additional_meta_price_group', 'sell_media_reprints_sf_term_meta', 99, 2 );


/**
 * Returns table cell markup for various filters
 *
 * @param $names (array) width|height|price
 * @param $names (array) width|height|price
 */
function sell_media_reprints_sf_table_cells( $names=null, $values=null ){
	$html = '
	<td>
		<input type="text" class="small-text" name="' . $names['width'] . '" value="' . $values['width'] . '" />
		<p class="description">'. __('Max Width','sell_media') . '</p>
	</td>
	<td>
		<input type="text" class="small-text" name="' . $names['height'] . '" value="' . $values['height'] . '" />
		<p class="description">'. __('Max Height','sell_media') . '</p>
	</td>
	<td>
		<span class="description">'. sell_media_get_currency_symbol() . '</span>
		<input type="text" class="small-text" name="' . $names['price'] . '" value="' . $values['price'] . '" />
		<p class="description">'. __('Price','sell_media') . '</p>
	</td>';
	return $html;
}

/**
 * Add item size on cart session.
 * @param  array $attrs Cart attrs.
 * @return array        Modified art attrs.
 */
function sell_media_reprints_cart_item_attrs( $attrs ){

	if( isset( $_POST['item_type'] ) && 'print' == $_POST['item_type'] ){
		$term = get_term( (int) $attrs['item_pgroup'], 'reprints-price-group' );
		if( $term )
			$attrs['item_size'] = $term->name;
	}

	return $attrs;

}

add_filter( 'sell_media_cart_item_attrs', 'sell_media_reprints_cart_item_attrs' );

/**
 * Localize shipping type and rates
 */
function sell_media_reprints_scripts_hook(){
	$settings = sell_media_get_plugin_options();

	$attr['reprints_shipping'] = $settings->reprints_shipping;

	if( 'shippingFlatRate' === $settings->reprints_shipping ){
		$attr['reprints_shipping_flat_rate'] = $settings->reprints_shipping_flat_rate;
	}

	if( 'shippingQuantityRate' === $settings->reprints_shipping ){
		$attr['reprints_shipping_flat_rate'] = $settings->reprints_shipping_quantity_rate;
	}

	if( 'shippingTotalRate' === $settings->reprints_shipping ){
		$attr['reprints_shipping_flat_rate'] = $settings->reprints_shipping_total_rate;
	}
	if($settings->reprints_free_shipping_threshold > 0) {
		$attr['reprints_free_shipping_threshold'] = $settings->reprints_free_shipping_threshold;
	}

	wp_localize_script( 'sell_media', 'sell_media_reprints', $attr );
}

add_action( 'sell_media_scripts_hook', 'sell_media_reprints_scripts_hook' );

/**
 * Add handling for paypal.
 * @param  int $amt Previous handling amount.
 * @return int      Modified handling amount.
 */
function sell_media_reprints_handling_cart( $amt ){
	global $sm_cart;
	$settings = sell_media_get_plugin_options();
	$items = $sm_cart->getItems();
	$subtotal = $sm_cart->getSubtotal();

	if( empty( $items ) )
		return $amt;

	$total_shipping = 0;

	// Get price of all items
	$total_qty = 0;
	$has_print = false;
	foreach ($items as $key => $item) {
		if( 'print' === $item['item_type'] ){
			$has_print = true;
			break;
		}
	}

	if ( $has_print ) {		
		if( 'shippingTotalRate' == $settings->reprints_shipping ){
			$total_shipping = intval( $subtotal ) * $settings->reprints_shipping_total_rate;
		}

		if( 'shippingFlatRate' == $settings->reprints_shipping ){
			$total_shipping = $settings->reprints_shipping_flat_rate;
		}

		if( 'shippingQuantityRate' == $settings->reprints_shipping ){
			foreach ($items as $key => $item) {
				if( 'print' === $item['item_type'] ){
					$total_qty+=$item['qty'];
				}
			}
			$total_shipping = intval( $total_qty ) * $settings->reprints_shipping_quantity_rate;
		}
		
	}
	// Check if the customer qualifies for free shipping.
	$blep = $settings->reprints_free_shipping_threshold;
	if( $blep < $subtotal ) {	
		$total_shipping = 0;
	}

	return number_format( doubleval( $total_shipping ), 2 );

}

add_filter( 'sell_media_payment_gateway_handling_cart', 'sell_media_reprints_handling_cart' );

/**
 * Check limit of item before add to cart.
 * @param  int Id of the item.
 * @param  double Price of the item.
 * @param  int Quantity to add in cart.
 * @param  array Attributes of the item.
 */
function sell_media_reprints_check_item_limit( $id, $price, $qty, $attrs ){
	if( !isset( $attrs['item_type'] ) ||  'print' != $attrs['item_type'] )
		return;

	$get_limit = sell_media_reprints_limited_edition_item_total_qty( $id );
	$item_cart_qty = sell_media_reprints_limited_edition_item_total_cart_qty( $id );
	if( false === $get_limit )
		return;
	$limit_exceeded = false;
	if( 0 >= $get_limit ){
		$limit_exceeded = true;
	}
	else if( $item_cart_qty >= $get_limit ){
		$limit_exceeded = true;
	}

	if( $limit_exceeded ){
		$response['code'] = '0';
		$response['message'] = __( 'Limit exceeded.', 'sell_media' );
		echo json_encode( $response );
		exit;
	}
}

add_action( 'sell_media_before_add_to_cart', 'sell_media_reprints_check_item_limit', 10, 4 );

/**
 * Update the item limit after payment success.
 * @param  int $payment_id Id of the payment.
 */
function sell_media_reprints_after_successful_payment( $payment_id ){
	$payments = new SellMediaPayments;
	$products = $payments->get_products( $payment_id );
	if( !empty( $products ) ){
		foreach ($products as $key => $product) {
			if( 'print' == $product['type'] ){
				$id = $product['id'];
				$current_limit = sell_media_reprints_limited_edition_item_total_qty( $id );
				$updated_limit = $current_limit - $product['qty'];
				update_post_meta( $id, '_sell_media_print_limited_edition_qty', $updated_limit );
			}
		}
	}
}

add_action( 'sell_media_after_successful_payment', 'sell_media_reprints_after_successful_payment' );

/**
 * Check limit before payment process
 */
function sell_media_reprint_before_payment_process(){
	if( !isset( $_POST['gateway'] ) )
		return;

	global $sm_cart;
	$cart_items = $sm_cart->getItems();
	if( !empty( $cart_items ) ){
		$limit_crossed = array();
		foreach ( $cart_items as $key => $item) {
			$id = $item['item_id'];
			if( isset( $item['item_type'] ) ||  'print' == $item['item_type'] ){
				$get_limit = sell_media_reprints_limited_edition_item_total_qty( $id );
				$item_cart_qty = sell_media_reprints_limited_edition_item_total_cart_qty( $id );
				if( false !== $get_limit ){
					$limit_exceeded = false;
					if( 0 >= $get_limit ){
						$limit_exceeded = true;
					}
					else if( $item_cart_qty > $get_limit ){
						$limit_exceeded = true;
					}

					if( $limit_exceeded ){
						$limit_crossed[$id] = $item;
						$limit_crossed[$id]['limit'] = $get_limit;
					}
				}

			}
		}
		if( !empty( $limit_crossed ) ){
			$message = "";
			foreach ($limit_crossed as $key => $item) {
				$message .= '- ';
				$message .= __( 'Item', 'sell_media' );
				$message .= " <strong>" . get_the_title( absint( $item['item_id'] ) ) . "</strong> ";
				$message .= __( 'has limit upto', 'sell_media' );
				$message .= " " .  $item['limit'];
				$message .= ".<br>";
			}
			$settings = sell_media_get_plugin_options();
			$checkout_page = "";
			if( isset( $settings->checkout_page ) && '' != $settings->checkout_page )
				$checkout_page = add_query_arg( array( 'clear' => rand() ), get_permalink( $settings->checkout_page ) ) ;
			$message .= '<a href="' .$checkout_page. '">' . __( 'Goto checkout page to fix it.', 'sell_media' ) . '</a>';

			wp_die( $message );
		}
	}
}

add_action( 'sell_media_before_payment_process', 'sell_media_reprint_before_payment_process' );

/**
 * Get total item quantity.
 * @param  int $id ID of the item.
 * @return mixed     Quantity of item.
 */
function sell_media_reprints_limited_edition_item_total_qty( $id ){
	$sell_media_print_limited_edition = get_post_meta( $id, '_sell_media_print_limited_edition', true );
	if( '1' != $sell_media_print_limited_edition ){
		return false;
	}

	$sell_media_print_limited_edition_qty = get_post_meta( $id, '_sell_media_print_limited_edition_qty', true );

	return ( 0 >= $sell_media_print_limited_edition_qty ) ? 0 : $sell_media_print_limited_edition_qty;
}

/**
 * Get total item quantity on cart
 * @param  int $id ID of the item.
 * @return mixed     Quanity of the item in cart.
 */
function sell_media_reprints_limited_edition_item_total_cart_qty( $id ){
	global $sm_cart;
	$cart_items = $sm_cart->getItems();
	$qty = 0;
	if( !empty( $cart_items ) ){
		foreach ( $cart_items as $key => $item) {
			if(
				isset( $item['item_type'] ) &&
				'print' == $item['item_type'] &&
				$id == $item['item_id']
			){
				$qty += $item['qty'];
			}
		}

	}

	return $qty;
}
// Popup.
/**
 * Add link to show shipped popup.
 *
 * @param  string $html Markup containing continue shopping in checkout page.
 * @return string       Return filtered markup including shipping process anchor tag.
 * @since  1.0.0
 */
function sell_media_reprints_shipping_process( $html  ) {
	echo  $html = sprintf( '<a href="#" id="shipping-process" >%s</a><br><br>', apply_filters( 'sell_media_shipping_process_text', __( 'Shipping Process', 'sell_media' ) ) );
}


/**
 * It holds the shipping process description which show in popup.
 *
 * @return void
 * @since  1.0.0
 */
function sell_media_reprints_shipping_process_popups() {
	$settings = sell_media_get_plugin_options();
	?>
    <div class=" sell-media-shipping-process-dialog-box">
        <div id="sell-media-shipping-process-dialog-box-target">
            <span class="close">X</span>
            <div class="content">
                <?php echo $settings->shipping_process; ?>
            </div>
        </div>
    </div>
<?php
}


add_action( 'init', 'sell_media_reprints_init' );

/**
	Init Functions.
 **/
function sell_media_reprints_init() {
	if ( is_admin() ) {
		return;
	}
	$has_print = false;
	global $sm_cart;
	if ( isset( $sm_cart ) ) {
		$items = @$sm_cart->getItems();
		foreach ( $items as $key => $item ) {
			if ( 'print' === $item['item_type'] ) {
				$has_print = true;
				break;
			}
		}
	}
	if ( $has_print ) {
		add_filter( 'sell_media_above_checkout_button', 'sell_media_reprints_shipping_process' );
		add_action( 'wp_footer', 'sell_media_reprints_shipping_process_popups' );
	}
}

/**
 * Add quick/ bulk edit custom fields.
 * @param  string $column_name Column name
 * @param  string $post_type   Post type name.
 */
function sell_media_quick_edit_reprints_price_group( $column_name, $post_type ) {
	if ( 'taxonomy-price-group' != $column_name ) return;
	global $post;
	$post_id = $post->ID;
	?>
	<fieldset class="inline-edit-col-left">
		<div class="inline-edit-col">
			<span class="title"><?php _e( 'Print Price Group', 'sell_media' ); ?></span>
			<?php
			$args = array(
				'show_option_none' => __( 'Select a pricelist', 'sell_media' ),
				'option_none_value' => 0,
				'name' => 'sell_media_reprints_price_group',
				'id' => 'sell-media-reprints-price-group',
				'class' => 'sell-media-reprints-price-group',
				'taxonomy' => 'reprints-price-group',
				'hierarchical' => true,
				'depth' => 1,
				'hide_empty' => false
			);
			$post_term = wp_get_post_terms( $post_id, 'reprints-price-group' );
			if ( isset( $post_term[0]->term_taxonomy_id ) && $post_term[0]->term_taxonomy_id > 0 ) {
				$args['selected'] = $post_term[0]->term_taxonomy_id;
			}
			
			wp_dropdown_categories( $args );
			wp_nonce_field( '_sell_media_quick_edit_reprints_nonce', 'sell_media_quick_edit_reprints_nonce' );
			?>
		</div>		
	</fieldset>
	<?php
}

add_action( 'quick_edit_custom_box', 'sell_media_quick_edit_reprints_price_group', 30, 2 );
add_action( 'bulk_edit_custom_box', 'sell_media_quick_edit_reprints_price_group', 30, 2 );

/**
 * Save quick edit values.
 * @param  int $post_id Post Id.
 */
function sell_media_save_quick_edit_reprints_custom_meta( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if ( ! isset( $_POST['sell_media_quick_edit_reprints_nonce'] ) || ! wp_verify_nonce( $_POST['sell_media_quick_edit_reprints_nonce'], '_sell_media_quick_edit_reprints_nonce' ) ) return;

	if ( ! current_user_can( 'edit_post', $post_id ) ) return;
	if ( wp_is_post_revision( $post_id ) ) return;
	if ( isset( $_POST['sell_media_reprints_price_group'] ) ) {
		wp_set_post_terms( $post_id, $_POST['sell_media_reprints_price_group'], 'reprints-price-group' );
	}
}

add_action( 'save_post', 'sell_media_save_quick_edit_reprints_custom_meta' );

/**
 * Save bulk edit values.
 */
function sell_media_save_bulk_edit_reprints() {

	if ( ! isset( $_POST['sell_media_quick_edit_reprints_nonce'] ) || ! wp_verify_nonce( $_POST['sell_media_quick_edit_reprints_nonce'], '_sell_media_quick_edit_reprints_nonce' ) ) return;
	$post_ids = ( ! empty( $_POST[ 'post_ids' ] ) ) ? $_POST[ 'post_ids' ] : array();
	$sell_media_price_group  = ( ! empty( $_POST[ 'sell_media_reprints_price_group' ] ) ) ? $_POST[ 'sell_media_reprints_price_group' ] : null;

	if ( ! empty( $post_ids ) && is_array( $post_ids ) ) {
		foreach( $post_ids as $post_id ) {
			wp_set_post_terms( $post_id, $sell_media_price_group, 'reprints-price-group' );
			
		}
	}

	die();
}

add_action( 'wp_ajax_sell_media_save_bulk_edit', 'sell_media_save_bulk_edit_reprints' );

/**
 * Selling preferences and pricelist fields which will display in add bulk.
 */
function sell_media_reprints_bulk_uploader_additional_fields() {
	?>
	<div id="sell-media-bulk-collection-field" class="sell-media-field">
		<label><?php esc_html_e( 'Selling preferences', 'sell_media' ); ?></label>
		<select name="_sell_media_reprints_sell" id="_sell_media_reprints_sell">
			<option value="both" selected="selected"><?php esc_html_e( 'Sell both', 'sell_media' ); ?></option>
            <option value="reprint"><?php esc_html_e( 'Sell print', 'sell_media' ); ?> </option>
            <option value="download"><?php esc_html_e( 'Sell downloads', 'sell_media' ); ?></option>
		</select>
	</div>

	<?php $sell_price_group = sell_media_settings_price_group( 'reprints-price-group' ); ?>
	<div id="sell-media-bulk-collection-field" class="sell-media-field">
	<label><?php esc_html_e( 'Pricelist for prints', 'sell_media' ); ?></label>
	<select name="sell_media_print_price_group" id="sell_media_print_price_group">
	<?php
	foreach ( $sell_price_group as $price_group_id => $price_group ) : ?>
		<option value="<?php esc_attr_e( $price_group_id, 'sell_media' ); ?>" ><?php esc_attr_e( $price_group['title'], 'sell_media' ); ?></option>
	<?php
	endforeach; ?>
	</select>
</div>
	<?php
}

add_action ( 'sell_media_bulk_uploader_additional_fields', 'sell_media_reprints_bulk_uploader_additional_fields' );

function sell_media_bulk_uloader_save_additional_fields( $post_id, $post_val ) {
	if ( isset( $post_val['_sell_media_reprints_sell'] ) ) {
		update_post_meta( $post_id, '_sell_media_reprints_sell', $post_val['_sell_media_reprints_sell'] );
	}

	if ( isset( $post_val['sell_media_print_price_group'] ) ) {
		wp_set_post_terms( $post_id, $post_val['sell_media_print_price_group'], 'reprints-price-group' );
	}
}

add_action ( 'sell_media_bulk_uploader_additional_fields_meta', 'sell_media_bulk_uloader_save_additional_fields', 10, 2 );