msgid ""
msgstr ""
"Project-Id-Version: Sell Media Reprints Self Fulfillment\n"
"POT-Creation-Date: 2013-12-26 16:33-0500\n"
"PO-Revision-Date: 2013-12-26 16:33-0500\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.5\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;"
"_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;"
"esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-SearchPath-1: .\n"

#: ../admin-tags.php:41 ../template-tags.php:223
msgid "Ship To"
msgstr ""

#: ../admin-tags.php:46
msgid "Additional Info"
msgstr ""

#: ../admin-tags.php:102
msgid "Total: "
msgstr ""

#: ../admin-tags.php:118
msgid "You just sold the following"
msgstr ""

#: ../admin-tags.php:119
msgid "Please ship to"
msgstr ""

#: ../admin-tags.php:120
msgid "Additional information can be found on the"
msgstr ""

#: ../admin-tags.php:131
msgid "New Sale!"
msgstr ""

#: ../admin-tags.php:193
msgid "Sell both"
msgstr ""

#: ../admin-tags.php:197
msgid "Sell print"
msgstr ""

#: ../admin-tags.php:201
msgid "Sell download"
msgstr ""

#: ../admin-tags.php:207
msgid "Price Group for Prints"
msgstr ""

#: ../admin-tags.php:212
msgid "Select a price group"
msgstr ""

#: ../admin-tags.php:220 ../sell-media-reprints-self-fulfillment.php:38
msgid "Selling Options"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:48
msgid ""
"Don't sell file downloads, only sell prints. You can override this on a per-"
"item basis on the Add/Edit Item page."
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:55
msgid "Unit of Measurement"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:56
msgid " The unit of measurement in which prints are sold."
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:65
msgid "Centimeters (cm)"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:69
msgid "Inches (in)"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:76
msgid "Base Region"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:88
msgid "Select Default Price Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:99
msgid "Price Groups"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:124
msgid "Prints"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:143
msgid "This plugin requires Sell Media to function properly"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:270
#: ../sell-media-reprints-self-fulfillment.php:284
msgctxt "sell_media"
msgid "Print Price Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:271
msgctxt "sell_media"
msgid "Print Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:272
msgctxt "sell_media"
msgid "Search Print Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:273
msgctxt "sell_media"
msgid "Popular Print Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:274
msgctxt "sell_media"
msgid "All Print Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:275
msgctxt "sell_media"
msgid "Parent Print Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:276
msgctxt "sell_media"
msgid "Parent Print  Group:"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:277
msgctxt "sell_media"
msgid "Edit Print Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:278
msgctxt "sell_media"
msgid "Update Print Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:279
msgctxt "sell_media"
msgid "Add New Print Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:280
msgctxt "sell_media"
msgid "New Print Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:281
msgctxt "sell_media"
msgid "Separate Print Group with commas"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:282
msgctxt "sell_media"
msgid "Add or remove Print Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:283
msgctxt "sell_media"
msgid "Choose from most used Print Group"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:342
msgid "Add print sizes, prices and shipping options below."
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:356
msgid ""
"Create a price group, then define specific print sizes, prices and shipping "
"options."
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:385 ../template-tags.php:66
msgid "Name"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:453
msgid "Max Width"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:457
msgid "Max Height"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:462 ../template-tags.php:68
msgid "Price"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:467
msgid "S&amp;H (Domestic)"
msgstr ""

#: ../sell-media-reprints-self-fulfillment.php:472
msgid "S&amp;H (Int'l)"
msgstr ""

#: ../template-tags.php:33 ../template-tags.php:41 ../template-tags.php:44
msgid "Download"
msgstr ""

#: ../template-tags.php:38 ../template-tags.php:42 ../template-tags.php:45
msgid "Print"
msgstr ""

#: ../template-tags.php:67
msgid "Size"
msgstr ""

#: ../template-tags.php:69
msgid "Qty"
msgstr ""

#: ../template-tags.php:70
msgid "Subtotal"
msgstr ""

#: ../template-tags.php:82
msgid "This item is not currently assigned a price group."
msgstr ""

#: ../template-tags.php:186
msgid "Description"
msgstr ""

#: ../template-tags.php:187
msgid "Price per reprint"
msgstr ""

#: ../template-tags.php:226
msgid "First Name"
msgstr ""

#: ../template-tags.php:230
msgid "Last Name"
msgstr ""

#: ../template-tags.php:234
msgid "Address 1"
msgstr ""

#: ../template-tags.php:238
msgid "Address 2"
msgstr ""

#: ../template-tags.php:242
msgid "City"
msgstr ""

#: ../template-tags.php:246
msgid "State"
msgstr ""

#: ../template-tags.php:250
msgid "Other Providence"
msgstr ""

#: ../template-tags.php:254
msgid "Country"
msgstr ""

#: ../template-tags.php:258
msgid "Postal Code"
msgstr ""

#: ../template-tags.php:262
msgid "Telephone"
msgstr ""

#: ../template-tags.php:266
msgid "Additional Information"
msgstr ""

#: ../template-tags.php:373
msgid "This item is available as a download only"
msgstr ""

#: ../template-tags.php:384
msgid "This item is available as a reprint only"
msgstr ""

#: ../template-tags.php:393
msgid " This item also available as a reprint"
msgstr ""
