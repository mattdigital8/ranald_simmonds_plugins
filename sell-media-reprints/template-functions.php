<?php


function sell_media_reprints_label( ) {
	$text = array(
			'print' => array( 
				'singular' => __( 'print', 'sell_media' ),
				'plural' => __( 'prints', 'sell_media' ),
			 )
		);
	return apply_filters( 'sell_media_reprints_text_labels', $text );
}
